$(document).ready(function(){
  var $carousel = $('.carousel.carousel-slider')
  var carouselIntervalId

  $carousel.carousel({
    fullWidth: true,
  });

  setCarouselInterval()

  $carousel.hover(handlerIn, handlerOut)

  function setCarouselInterval () {
    carouselIntervalId = setInterval(function () {
      $carousel.carousel('next')
    }, 3000);
  }

  function handlerIn () {
    clearInterval(carouselIntervalId)
  }

  function handlerOut () {
    setCarouselInterval()

  }

  $(".dropdown-button").dropdown();

  // Search page
  var searchTerm = window.location.search.split('=')[1]
  $('[data-dynamic-search]').text(searchTerm)
});
